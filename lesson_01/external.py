import json
import time
import random


def get_segment_from_crm():
    time.sleep(2)
    response = {
        "status": "error",
        "code": 404,
        "message": "Unable to connect"
    }
    moon_phase = random.randint(0, 5)

    if moon_phase == 4:
        response["status"] = "ok"
        response["code"] = 200
        response["data"] = {
            "segment": "kg_2021_01",
            "users": [
                {"name": "Petka", "email": "petka@unicef.org", "tags": json.dumps({"gender": "male",
                                                                                   "phone": "+5 321 654"})},
                {"name": "Vaska", "email": "vaska@fbr.org", "tags": json.dumps({"gender": "male",
                                                                                "phone": "+7 451 123"})},
                {"name": "Afanasy", "email": "nagibator9000@keydjibee.org", "tags": json.dumps({"gender": "other",
                                                                                                "phone": "+13 666 420"})}
            ]
        }
    return response


def set_tag(userid, name, value):
    response = {
        "status": "error",
        "code": 404,
        "message": "Tag {} not found".format(name)
    }
    if userid not in ["petka@unicef.org", "vaska@fbr.org", "nagibator9000@keydjibee.org"]:
        response["message"] = "User {} not found".format(userid)
        return response

    if (name == "kg_2021_01" and type(value) == bool) \
            or (name == "gender" and value in ["male", "female", "other"]) \
            or (name == "phone" and value):
        response["status"] = "ok"
        response["code"] = "200"
        response["message"] = "User {}: Value {} successfully set for tag {}".format(userid, value, name)
    else:
        response["code"] = 401
        response["message"] = "Unsupported value {} for tag {}".format(value, name)

    return response
