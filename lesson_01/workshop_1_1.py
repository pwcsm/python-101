
RETURN_CODES = (200, 210, 400, 401, 403, 404)

email_list = ["john.doe@mail.tld", "anna.frank@mail.tld", "frank.duval@mail.tld"]

contacts = {
    "john.doe@mail.tld": "+7 666 420 1399",
    "anna.frank@mail.tld": "+7 777 444 1566",
    "frank.duval@mail.tld": "+7 222 228 1512"
}

for code in RETURN_CODES:
    print(code)

for email in email_list:
    print(email)

for contact in contacts.keys():
    print("{} - {}".format(contact, contacts[contact]))
