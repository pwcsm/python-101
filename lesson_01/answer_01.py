# And that's a possible answer to boss_01 challenge.
import time
import json
import sys
import os

cwd = os.path.abspath(os.getcwd())
sys.path.append(os.path.join(cwd, 'external.py'))

from external import get_segment_from_crm, set_tag

# let's call the remote API to pull the segment from the CRM
# the loop will run until interrupted via CMD-C or until success
while True:
    print("Attempting to get a segment from CRM...")
    response = get_segment_from_crm()
    if response["code"] == 200:
        print("Success!!!")
        print(response)
        break
    else:
        print("Error :(")
        print(response)
        print("Will retry in 1 second")
        time.sleep(1)

# Let's get the useful part of the response's payload
data = response.get("data", None)

# If it's not empty, let's do the magic
if data and data.get("users"):
    # each user goes as an element of the list data["users"]
    for user in data.get("users"):
        r = set_tag(user["email"], "kg_2021_01", True)
        print(r)
        # tags are, in fact, JSON string, not a dictionary, so we have to de-serialize it from string to dict:
        tags = json.loads(user["tags"])
        r = set_tag(user["email"], "gender", tags["gender"])
        print(r)
