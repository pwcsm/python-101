# And that's your first boss. Kill it with fire!

"""
Your colleague has recently gone for a well-deserved vacation, but just before he left, he'd told you that
one customer requested to import tags by user ID using their CRM data as the source for value.

The colleague said: "I've almost completed the script, just couple of 'if' statements, and it'll work.
The CRM is unstable, though, it gives errors from time to time, please make sure the script works all the time"
And left off to the sunset.

You are to complete the script and make sure that tags (gender and a custom tag "kg_2021_01") are set successfully.

Here is what the customer wrote to your colleague:
--
Dear Max,

Thanks a lot for promising to update tags by tomorrow morning, it's really urgent - we plan a campaign tomorrow,
where we'll use the segment to target our message.

Please set "kg_2021_01" = True for all users that our CRM provides via API get_segment_from_crm().
And also set a corresponding gender tor each of those users.

Cheers,
Veronika
--

That, the below piece of code and some comments - that is all you got, besides your talent and Google :)
"""
import sys
import os
import time
import json

cwd = os.path.abspath(os.getcwd())
sys.path.append(os.path.join(cwd, 'external.py'))

from external import get_segment_from_crm, set_tag

# let's call the remote API to pull the segment from the CRM
print("Attempting to get a segment from CRM...")
response = get_segment_from_crm()
# FIXME: ^ the API call gives an error in about 80% of time, so I'll rewrite the code to retry until the code is 200.

if response["code"] == 200:
    print("Success!!!")
    print(response["data"])
else:
    print("It didn't work :(")
    print(response)

data = response.get("data", None)

# I wasn't able to get tag values, it always gives an error :(
# Pls get tag values somehow, the rest of the code should work fine
if data and data.get("users"):
    for user in data.get("users"):
        r = set_tag(user["email"], "kg_2021_01", True)
        print(r)
        tags = user["tags"]
        # the below line doesn't work for some reason :(
        r = set_tag(user["email"], "gender", tags["gender"])
        print(r)
