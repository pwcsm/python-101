import csv
import json

with open('sample_1.csv', 'r+') as f:
    rows = csv.reader(f, delimiter=',', quotechar='"')
    for count, row in enumerate(rows):
        if count:
            print(row[0], row[1])


# task 1: print device type for corresponding hwid
# task 2: write new csv with header, where each user ID has session count next to it
# task 3: write new csv with summary of session count and count of devices with push token per platform